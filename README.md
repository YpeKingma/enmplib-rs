# enmplib-rs

Library of rust code shared between the rust programs in
https://gitlab.com/enmprivaggr/enmprv

## Licence

MIT licence
