use std::convert::TryFrom;

use ::bincode::config::{Bounded, VarintEncoding, WithOtherIntEncoding, WithOtherLimit};
use ::bincode::DefaultOptions;
use ::bincode::Options;
use ::bitvec::prelude::{BitSlice, BitVec, Lsb0};
pub use ::enum_iterator::IntoEnumIterator;
use ::serde::{Deserialize, Serialize};

const BIN_CODE_SIZE_LIMIT: u64 = 40000;

pub fn get_bincode_options(
) -> WithOtherLimit<WithOtherIntEncoding<DefaultOptions, VarintEncoding>, Bounded> {
    DefaultOptions::new()
        .with_varint_encoding()
        .with_limit(BIN_CODE_SIZE_LIMIT)
}

#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Eq, Hash, IntoEnumIterator)]
#[allow(non_camel_case_types)]
pub enum MeasurementType {
    El_kWh,  // Electric, kWh
    El_kW,   // Electric, kW
    NgNl_m3, // Natural Gas, NL, 9.769 kWh per normalized m3.
}

impl MeasurementType {
    pub fn to_str(self) -> &'static str {
        match self {
            MeasurementType::El_kWh => "el.kWh",
            MeasurementType::El_kW => "el.kW",
            MeasurementType::NgNl_m3 => "ng.nl.m3",
        }
    }

    pub fn to_usize(self) -> usize {
        // CHECKME: IntoEnumIterator could provide this without linear search
        let mut ord: usize = 0;
        for mt in MeasurementType::into_enum_iter() {
            if mt == self {
                return ord;
            } else {
                ord += 1;
            }
        }
        unreachable!();
    }
}

pub fn measurement_types_bit_vec() -> BitVec<Lsb0, u8> {
    let mut res = BitVec::<Lsb0, u8>::with_capacity(MeasurementType::VARIANT_COUNT);
    res.resize(MeasurementType::VARIANT_COUNT, false);
    res
}

pub fn bit_slice(bytes: &[u8]) -> &BitSlice<Lsb0, u8> {
    BitSlice::<Lsb0, u8>::from_slice(bytes).unwrap()
}

pub fn set_measurement_type_bit(
    msrmnt_types_bit_vec: &mut BitSlice<Lsb0, u8>,
    msrmnt_type: MeasurementType,
    val: bool,
) {
    *msrmnt_types_bit_vec
        .get_mut(msrmnt_type.to_usize())
        .unwrap() = val;
}

pub fn get_measurement_type_bit(
    msrmnt_types_bit_vec: &BitSlice<Lsb0, u8>,
    msrmnt_type: MeasurementType,
) -> bool {
    *msrmnt_types_bit_vec.get(msrmnt_type.to_usize()).unwrap()
}

impl TryFrom<&str> for MeasurementType {
    type Error = String;
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        for mt in MeasurementType::into_enum_iter() {
            if value == mt.to_str() {
                return Ok(mt);
            }
        }
        Err(format!("MeasurementType unknown {}", value))
    }
}

impl TryFrom<usize> for MeasurementType {
    type Error = String;
    fn try_from(value: usize) -> Result<Self, String> {
        // CHECKME: IntoEnumIterator could provide this without linear search
        let mut ord = 0;
        for mt in MeasurementType::into_enum_iter() {
            if ord == value {
                return Ok(mt);
            } else {
                ord += 1;
            }
        }
        Err(format!("MeasurementType usize too big {}", value))
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum IncrementalRequest {
    Partial, // start a request, or add to a started request, do not reply.
    Final,   // add to an earlier request, if any, and provide the reply
}

#[derive(Serialize, Deserialize, Debug)]
pub struct EquipmentIdMeasTimeTypesBytes {
    pub eq_id: Vec<u8>,
    pub meas_dt: Vec<u8>,
    // big endian bytes of bitvec with bits at positions MeasurementType.to_usize():
    pub msrmnt_types_vec_u8: Vec<u8>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct EncKeyAddRequest {
    pub request_stage: IncrementalRequest,
    pub eq_id_meas_dt_types_vec: Vec<EquipmentIdMeasTimeTypesBytes>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MeasurementTypeTotalEncKey {
    pub msrmnt_type: MeasurementType,
    pub total_encryption_key: String, // decimal digits, with optional sign and decimal point
    pub total_measurements: u64,
    pub refused_eq_id_vec: Vec<Vec<u8>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct EncKeyAddReply {
    pub msrmnt_type_total_key_vec: Vec<MeasurementTypeTotalEncKey>,
}
