use std::fs::{remove_file, File};
use std::io::{Read, Write};
use std::net::{TcpStream, UdpSocket};
use std::path::Path;
use std::process::{Command, Output};
use std::thread;
use std::time::{Duration, Instant};

use ::blake2::{Blake2s, Digest};
use ::num::{bigint::Sign, BigInt, Integer};

use super::bincoded::MeasurementType;

pub fn str_concat(parts: &[&str]) -> String {
    let mut res = String::new();
    for part in parts {
        res.push_str(part)
    }
    res
}

pub fn str_join(joiner: &str, parts: &[&str]) -> String {
    str_join_iter(joiner, &mut parts.iter().copied())
}

pub fn str_join_iter(joiner: &str, parts: &mut dyn Iterator<Item = &str>) -> String {
    let mut res = String::new();
    if let Some(first) = parts.next() {
        res.push_str(first);
        for later in parts {
            res.push_str(joiner);
            res.push_str(later);
        }
    }
    res
}

pub fn bash_cmd_output(cmd: &str) -> Result<Output, String> {
    match Command::new("/bin/bash").arg("-c").arg(cmd).output() {
        Err(e) => Err(format!("{} {:?}", cmd, e)),
        Ok(output) => Ok(output),
    }
}

fn bash_cmd_stdout(cmd: &str) -> Result<String, String> {
    let result = bash_cmd_output(cmd)?;
    if !result.status.success() {
        Err(format!(
            "cmd {} result.status.success() is false {:?}",
            cmd, result
        ))
    } else if let Ok(s) = std::str::from_utf8(&result.stdout) {
        Ok(s.to_string())
    } else {
        Err(format!(
            "from_utf8 failed on result.stdout, cmd {} result {:?}",
            cmd, result
        ))
    }
}

pub fn wait_clock_synchronized() -> Result<(), String> {
    let start_time = Instant::now();
    let cmd = "timedatectl | grep 'synchronized: yes'";
    let max_seconds = 45;
    loop {
        let result = bash_cmd_output(cmd)?;
        if result.status.success() {
            return if std::str::from_utf8(&result.stdout).is_ok() {
                Ok(())
            } else {
                Err(format!("cmd {} non utf8 output: {:?}", cmd, &result.stdout))
            };
        }
        if start_time.elapsed().as_secs() > max_seconds {
            return Err(format!(
                "clock not synchronized after {} seconds",
                max_seconds
            ));
        }
        println!("waiting for clock synchronization");
        thread::sleep(Duration::new(5, 0));
    }
}

pub fn addr_port_str(ip4_addr: &str, port: &str) -> String {
    str_join(":", &[ip4_addr, port])
}

pub fn udp_bind(local_addr_port: &str) -> Result<UdpSocket, String> {
    match UdpSocket::bind(local_addr_port) {
        Err(e) => Err(format!(
            "UdpSocket::bind {} for {}, is the wg tunnel up?",
            e, local_addr_port
        )),
        Ok(udp_socket) => Ok(udp_socket),
    }
}

pub fn ip4_addr_in_wg_intf(ip4_addr: &str) -> Result<bool, String> {
    let mut intf_names = Vec::new();
    // From the ip output lines starting with 1: 2: etc, cut all but the 2nd word:
    let cmd = "ip link show | grep -e '^[0-9]\\+:' | cut --delimiter=' ' -f2";
    let stdout = bash_cmd_stdout(cmd)?;
    for l in stdout.split('\n') {
        // remove the ending colon
        if let Some(cp) = l.find(':') {
            if cp + 1 == l.len() {
                intf_names.push(l[..cp].to_string().clone());
            }
        }
    }
    let mut wg_intf_names = Vec::new();
    for intf_name in &intf_names {
        // ip -d shows interface details, o.a. the interface type, which should be wireguard
        let cmd = &format!("ip -d link show {} | grep wireguard", intf_name);
        let result = bash_cmd_output(cmd)?;
        if result.status.success() {
            wg_intf_names.push(intf_name);
        }
    }
    for wg_intf_name in &wg_intf_names {
        dbg!(wg_intf_name);
        let cmd = &format!("ip -d addr show {} | grep 'inet '", wg_intf_name);
        let stdout = bash_cmd_stdout(cmd)?;
        let mut inet_found = false;
        for w in stdout.split(' ') {
            if w == "inet" {
                inet_found = true;
            } else if inet_found {
                dbg!(w);
                let wg_local_address = if let Some(slash) = w.find('/') {
                    // CHECKME: use mask w[slash+1..] to check ip4_address against wg_local_address?
                    &w[..slash]
                } else {
                    w
                };
                dbg!(wg_local_address);
                if wg_local_address == ip4_addr {
                    return Ok(true);
                }
                break; // next wg_intf_name
            }
        }
    }
    Ok(false)
}

pub fn send_version_size_message(message: &[u8], tcp_stream: &mut TcpStream) -> Result<(), String> {
    let mes_size = message.len();
    if mes_size > u16::MAX.into() {
        Err(format!("message too long: {}", mes_size))
    } else {
        // prefix 2 zero version bytes and 2 length bytes
        let mut version_size: [u8; 4] = [0; 4];
        version_size[2] = mes_size as u8;
        version_size[3] = (mes_size >> 8) as u8;
        if let Err(e) = tcp_stream.write_all(&version_size) {
            Err(format!("write_all version_size {:?}", e))
        } else if let Err(e) = tcp_stream.write_all(message) {
            Err(format!("write_all message {:?}", e))
        } else {
            Ok(())
        }
    }
}

pub fn receive_version_size_message(tcp_stream: &mut TcpStream) -> Result<Vec<u8>, String> {
    let mut version_size: [u8; 4] = [0; 4];
    if let Err(e) = tcp_stream.read_exact(&mut version_size) {
        return Err(format!("read_exact version_size {:?}", e));
    }
    if version_size[0] != 0 || version_size[1] != 0 {
        return Err(format!("unexpected version bytes {:?}", &version_size[..2]));
    }
    let message_size = version_size[2] as usize + ((version_size[3] as usize) << 8);
    println!(
        "info: receive_version_size_message message_size {}",
        message_size
    );
    let mut message = Vec::<u8>::new();
    message.resize(message_size, 0);
    if let Err(e) = tcp_stream.read_exact(&mut message) {
        Err(format!("read_exact message {:?}", e))
    } else {
        Ok(message)
    }
}

fn thread_id_string() -> String {
    let thread_id_dbg = format!("{:?}", thread::current().id());
    let open_pos = thread_id_dbg.find('(').unwrap();
    let close_pos = thread_id_dbg.find(')').unwrap();
    thread_id_dbg[open_pos + 1..close_pos].to_string()
}

fn expect_empty(title: &str, mes: &[u8]) -> Result<(), String> {
    if mes.is_empty() {
        Ok(())
    } else if let Ok(s) = std::str::from_utf8(mes) {
        Err(format!("{} non empty: {}", title, s))
    } else {
        Err(format!("{} non utf8: {:?}", title, mes))
    }
}

fn rm_file(file_path: &Path) -> Result<(), String> {
    match remove_file(file_path) {
        Ok(()) => Ok(()),
        Err(e) => Err(format!("{:?} at remove_file {:?}", e, file_path)),
    }
}

const TEMP_DIR_NAME: &str = "/tmp/";
const SIG_FILE_SUFFIX: &str = "sig";
const DATA_FILE_SUFFIX: &str = "data";

fn thread_unique_tmp_file_name(temp_files_prefix: &str, suffix: &str) -> String {
    str_concat(&[temp_files_prefix, &thread_id_string(), suffix])
}

pub struct OpenSslSigner {
    private_key_file_name: String,
    key_pass_phrase: String,
    temp_files_prefix: String,
}

impl OpenSslSigner {
    pub fn new(
        private_key_file_name: &str,
        key_pass_phrase: &str,
        process_unique_name_part: &str, // should distinguish this process.
    ) -> Self {
        OpenSslSigner {
            private_key_file_name: private_key_file_name.to_string(),
            key_pass_phrase: key_pass_phrase.to_string(),
            temp_files_prefix: str_concat(&[TEMP_DIR_NAME, process_unique_name_part]),
        }
    }

    pub fn generate_signature(&self, data: &[u8]) -> Result<Vec<u8>, String> {
        let name = "generate_signature";
        let data_file_name = thread_unique_tmp_file_name(&self.temp_files_prefix, DATA_FILE_SUFFIX);
        {
            let mut data_file;
            match File::create(&data_file_name) {
                Ok(f) => data_file = f,
                Err(e) => return Err(format!("{:?} at File::create {}", e, data_file_name)),
            }
            if let Err(e) = data_file.write_all(data) {
                return Err(format!("{:?} at write_all {}", e, data_file_name));
            }
        }
        let sig_file_name = thread_unique_tmp_file_name(&self.temp_files_prefix, SIG_FILE_SUFFIX);
        let cmd = str_join(
            " ",
            &[
                "openssl dgst -sha256 -sign",
                &self.private_key_file_name,
                "-passin",
                &format!("pass:{}", self.key_pass_phrase),
                "-out",
                &sig_file_name,
                &data_file_name,
            ],
        );
        let result = bash_cmd_output(&cmd)?;
        expect_empty(&format!("{} stdout", name), &result.stdout)?;
        expect_empty(&format!("{} stderr", name), &result.stderr)?;
        let mut signature = Vec::<u8>::new();
        {
            let mut sig_file;
            match File::open(&sig_file_name) {
                Ok(f) => sig_file = f,
                Err(e) => return Err(format!("{:?} at File::open {}", e, sig_file_name)),
            }
            if let Err(e) = sig_file.read_to_end(&mut signature) {
                return Err(format!("{:?} at read_to_end {}", e, sig_file_name));
            }
        }
        rm_file(Path::new(&sig_file_name))?;
        rm_file(Path::new(&data_file_name))?;
        if result.status.success() {
            Ok(signature)
        } else {
            Err(format!("cmd: {} status: {:?}", cmd, result.status))
        }
    }
}

pub struct OpenSslSignatureVerifier {
    public_key_file_name: String,
    temp_files_prefix: String,
}

impl OpenSslSignatureVerifier {
    pub fn new(
        public_key_file_name: &str,
        process_unique_name_part: &str, // should distinguish this process
    ) -> Self {
        OpenSslSignatureVerifier {
            public_key_file_name: public_key_file_name.to_string(),
            temp_files_prefix: str_concat(&[TEMP_DIR_NAME, process_unique_name_part]),
        }
    }

    pub fn verify_signature(&self, signed_data: &[u8], signature: &[u8]) -> Result<bool, String> {
        let name = "verify_signature";
        let sig_file_name = thread_unique_tmp_file_name(&self.temp_files_prefix, SIG_FILE_SUFFIX);
        {
            let mut sig_file;
            match File::create(&sig_file_name) {
                Ok(f) => sig_file = f,
                Err(e) => return Err(format!("{:?} at File::create {}", e, sig_file_name)),
            }
            if let Err(e) = sig_file.write_all(signature) {
                return Err(format!("{:?} at write_all {}", e, sig_file_name));
            }
        }
        let data_file_name = thread_unique_tmp_file_name(&self.temp_files_prefix, DATA_FILE_SUFFIX);
        {
            let mut data_file;
            match File::create(&data_file_name) {
                Ok(f) => data_file = f,
                Err(e) => return Err(format!("{:?} at File::create {}", e, data_file_name)),
            }
            if let Err(e) = data_file.write_all(signed_data) {
                return Err(format!("{:?} at write_all {}", e, data_file_name));
            }
        }
        let cmd = str_join(
            " ",
            &[
                "openssl dgst -sha256 -verify",
                &self.public_key_file_name,
                "-signature",
                &sig_file_name,
                &data_file_name,
            ],
        );
        let result = bash_cmd_output(&cmd)?;
        rm_file(Path::new(&sig_file_name))?;
        rm_file(Path::new(&data_file_name))?;
        let title_stdout = &format!("{} stdout", name);
        let title_stderr = &format!("{} stderr", name);
        if result.status.success() {
            if *"Verified OK\n".as_bytes() == result.stdout {
                expect_empty(title_stderr, &result.stderr)?;
                Ok(true)
            } else {
                expect_empty(title_stdout, &result.stdout)?;
                expect_empty(title_stderr, &result.stderr)?;
                Ok(false)
            }
        } else {
            expect_empty(title_stdout, &result.stdout)?;
            expect_empty(title_stderr, &result.stderr)?;
            Err(format!("cmd: {} status: {:?}", cmd, result.status))
        }
    }
}

pub fn right(s: &str, width: u16) -> String {
    let mut r = String::new();
    for _ in 0..((width as i32) - (s.len() as i32)) {
        r.push(' ');
    }
    r.push_str(s);
    r
}

pub fn home_path(path: &str) -> String {
    if path.starts_with('/') {
        path.to_string()
    } else {
        str_concat(&[&std::env::var("HOME").unwrap(), "/", path])
    }
}

pub fn encryption_key(
    seed: &[u8],
    eq_id: &[u8],
    meas_dt: &[u8],
    msrmnt_type: MeasurementType,
    enc_modulus: &BigInt,
) -> BigInt {
    let msrmnt_bytes = msrmnt_type.to_str().as_bytes();
    let mut hasher = Blake2s::new();
    for part in &[seed, eq_id, msrmnt_bytes, meas_dt] {
        hasher.update(part);
    }
    BigInt::from_bytes_be(Sign::Plus, &hasher.finalize()).mod_floor(enc_modulus)
}
